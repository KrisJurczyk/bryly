﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shape.interfaces;

namespace PorównywanieBrył._10._03
{
    class ConsoleDiemensionProvider : IDimensionsProvider//tu jest problem bo odpytuje usera, zrobić z linku
    {
        public double GetDimension(string name)
        {
            Console.WriteLine($"Podaj wymiar: {name}");
            return Convert.ToDouble(Console.ReadLine());
        }
    }
}
