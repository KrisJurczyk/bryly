﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shape.interfaces;

namespace Shape
{
    public static class DPExtensionMethods//zmienic na voida z opisem co jest nie tak
    {
        public static bool CheckIfAllPositive(this IDimensionsProvider dp, params string[] t)
        {
            if (dp == null) return false;
            foreach (var x in t) //petla z parametrem x
            {
                var z = dp.GetDimension(x);
                if (z < 0) return false;
                if (double.IsNaN(z)) return false; //czy jest NaN
            }
            return true;
        }
    }
}
