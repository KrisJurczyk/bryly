﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Shape.interfaces;

namespace Shape
{
    internal class DummyDimensionProvider : IDimensionsProvider
    {
        private readonly IReadOnlyDictionary<string, double> dict;

        public DummyDimensionProvider(IReadOnlyDictionary<string, double> dict)
        {
            this.dict = dict;
        }

        public double GetDimension(string edge)
        {
            return dict[edge];
        }
    }
}
