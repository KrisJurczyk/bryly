﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Shape
{
    class PriThr : Prism, IRectangularBase, IComparable<PriThr>
    {
        /*private readonly double _a;
        private readonly double _b;*///pola

        public PriThr(double height, double a, double b) : base(height)
        {
            this.A = a;
            this.B = b;
        }

        protected override double GetBaseArea()
        {
            return A *B / 2;
        }

        public double A { get; }
        public double B { get; }

        public int ComerTo(PriThr other)
        {
            return other == null 
                ? 1 
                : GetVolume().CompareTo(other.GetVolume());
        }

        public int CompareTo(PriThr other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var aComparison = A.CompareTo(other.A);
            if (aComparison != 0) return aComparison;
            return B.CompareTo(other.B);
        }
    }
}
