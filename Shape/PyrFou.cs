﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shape
{
    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    public class PyrFou : Pyramid, IRectangularBase
    {
        private double _a;
        private double _b;

        public PyrFou(double height) : base(height)
        {
        }

        protected override double GetBaseArea()
        {
            return A * B;
        }

        public double A
        {
            get { return _a; }
        }

        public double B
        {
            get { return _b; }
        }
    }
}
