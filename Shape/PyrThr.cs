﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shape
{
    class PyrThr : Pyramid
    {
        public double A { get; set; }

        public double B { get; set; }

        public PyrThr(double height) : base(height)
        {
        }

        protected override double GetBaseArea()
        {
            return A * B / 2;
        }
    }
}
