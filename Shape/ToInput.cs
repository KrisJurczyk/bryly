﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shape
{
    public class ToInput
    {
        public bool Check<T>(T inner, T outer) where T : Shape, IRectangularBase
        {
            return inner.A < outer.A && inner.B < outer.B && inner.Height < outer.Height;
        }
    }
}
