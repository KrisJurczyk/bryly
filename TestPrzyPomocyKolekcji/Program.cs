﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shape;

namespace TestPrzyPomocyKolekcji
{
    class Program
    {
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            ISet<PriFou> set = new SortedSet<PriFou>();//unika powt
            var a = new PriFou(1, 2, 3);
            var b = new PriFou(3, 2, 1);
            var c = new PriFou(3, 2, 1);
            var d = new PriFou(6, 1, 1);
            var e = new PriFou(7, 10, 10);
            var f = new PriFou(0.1, 0.2, 0.1);
            a.CompareTo(null);
            set.Add(a);
            set.Add(b);
            set.Add(c);
            set.Add(d);
            set.Add(d);
            set.Add(e);
            set.Add(f);
            Debug.Print("dodany");
            foreach (var priFou in set)
            {
                Console.WriteLine($"{priFou.A} {priFou.B} wysokość: {priFou.Height} {priFou.GetVolume()}");
            }
            Console.WriteLine(set.Contains(a));
            Console.WriteLine(set.Contains(f));
            Console.WriteLine(set.Contains(new PriFou(2, 3, 1)));
        }
    }
}
